// Our services - tabs

const tab = function () {
  const tabNavigation = document.querySelectorAll(".tab-item");
  const tabContent = document.querySelectorAll(".services-tab-block");

  function getTabContent(tabName) {
    tabContent.forEach((elem) => {
      if (elem.classList.contains(tabName)) {
        elem.classList.add("active-tab");
      } else {
        elem.classList.remove("active-tab");
      }
    });
  }

  function selectTabNavigation() {
    tabNavigation.forEach((elem) => {
      elem.classList.remove("hover-tab");
    });
    this.classList.add("hover-tab");
    const tabName = this.getAttribute("data-tab-name");
    getTabContent(tabName);
  }

  tabNavigation.forEach((elem) => {
    elem.addEventListener("click", selectTabNavigation);
  });
};

// Amazing work

const loadMore = () => {
  const button = document.querySelector(".load-more-btn");
  button.addEventListener("click", (event) => {
    event.target.parentElement.remove();
    const container = document.querySelector(".amazing-work-images");
    container.innerHTML += container.innerHTML;
    [...container.children].forEach((element, index) => {
      if (index <= 11) {
        return;
      }
      const number = Math.floor(Math.random() * 7) + 1;
      const img = element.querySelector("img");
      img.src = `./images/Step Project Ham/landing page/landing-page${number}.jpg`;
    });
  });
};

function filter() {
  const listElement = document.querySelectorAll(".menu-item");
  listElement.forEach((elem) => {
    elem.addEventListener("click", (event) => {
      const category = event.target.getAttribute("data-category");
      const images = document.querySelectorAll(".amazing-work-img-item");
      images.forEach((elem) => {
        const elementCategory = elem.getAttribute("data-category");
        if (category === elementCategory || category === "all") {
          elem.style.display = "block";
        } else {
          elem.style.display = "none";
        }
      });
    });
  });
}

// What people say about Ham

const userTab = function () {
  const userContent = document.querySelectorAll(".review-container");
  const userNav = [...document.querySelectorAll(".nav-item-to-select")];

  const selectElement = (elementToSelect) => {
    userNav.forEach((elem) => {
      elem.classList.remove("active-photo");
    });
    elementToSelect.classList.add("active-photo");
    const userName = elementToSelect.getAttribute("data-user");
    userContent.forEach((content) => {
      const contentUserName = content.getAttribute("data-user");
      if (contentUserName === userName) {
        content.style.display = "flex";
      } else {
        content.style.display = "none";
      }
    });
  };

  function selectUserNav() {
    selectElement(this);
  }

  userNav.forEach((elem) => {
    elem.addEventListener("click", selectUserNav);
  });

  document.querySelector(".nav-button-left").addEventListener("click", () => {
    const selectedNavIndex = userNav.findIndex((element) =>
      element.classList.contains("active-photo")
    );
    const nextElementToSelect = userNav[selectedNavIndex - 1];
    if (nextElementToSelect) {
      selectElement(nextElementToSelect);
    }
  });

  document.querySelector(".nav-button-right").addEventListener("click", () => {
    const selectedNavIndex = userNav.findIndex((element) =>
      element.classList.contains("active-photo")
    );
    const nextElementToSelect = userNav[selectedNavIndex + 1];
    if (nextElementToSelect) {
      selectElement(nextElementToSelect);
    }
  });
};

tab();
loadMore();
filter();
userTab();
